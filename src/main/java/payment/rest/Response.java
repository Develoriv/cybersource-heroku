package payment.rest;

public class Response {

    public class CybersourcePaymentResponse {

        public ClientReferenceInformation clientReferenceInformation;
        public String id;
        public OrderInformation orderInformation;
        public PaymentAccountInformation paymentAccountInformation;
        public PaymentInformation paymentInformation;
        public ProcessorInformation processorInformation;
        public String reconciliationId;
        public String status;
        public String submitTimeUtc;
        public Links _links;
    }

    public class ClientReferenceInformation {
        public String code;
    }

    public class OrderInformation {
        public AmountDetails amountDetails;
    }

    public class AmountDetails {
        public String authorizedAmount;
        public String currency;
    }

    public class PaymentAccountInformation {
        public Card card;
    }

    public class PaymentInformation {
        public TokenizedCard tokenizedCard;
    }

    public class Card {
        public String type;
    }

    public class TokenizedCard {
        public String type;
    }

    public class ProcessorInformation {
        public String approvalCode;
        public String responseCode;
        public Avs avs;
    }

    public class Avs {
        public String code;
        public String codeRaw;
    }

    public class Links {
        public Self self;
        public Self authReversal;
        public Self capture;
        public Self refund;
        public Self void_x; //void in JSON
    }

    public class Self {
        public String href;
        public String method;
    }
}
