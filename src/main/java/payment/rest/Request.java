package payment.rest;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

@RestController
@RequestMapping(path = "/response")
public class Request
{

    public static String merchantKeyId;
    public static String merchantsecretKey;
    public static String merchantId;

    /* Sandbox Host: apitest.cybersource.com
     * Production Host: api.cybersource.com
     */
    public static String requestHost = "apitest.cybersource.com";

    private final String USER_AGENT = "Mozilla/5.0";

    public static String gmtDateTime = "DATE_PLACEHOLDER";
    public static String postRequestTarget = "REQUEST_TARGET_PALCEHOLDER";
    public static String APINAME = "APINAME_PLACEHOLDER";
    public static String resource = "resource_PLACEHOLDER";

    public static String  payload = null;

    public static String  responseJSON;

    @PostMapping(path= "/", consumes = "application/json", produces = "application/json")
    public String processPayment(@RequestBody Payment payment) throws Exception
    {
        System.out.println("payment input: "+payment);

        merchantKeyId = Payment.keyId;
        merchantsecretKey = Payment.secretKey;
        merchantId = Payment.merchantId;

        Request http = new Request();

        Body body = new Body();
        String requestBodyString;

        //build the Amount Details
        AmountDetails amountDetail = new AmountDetails(payment.totalAmount,payment.currency);

        //build the Billing Details
        BillTo billTo = new BillTo(payment.firstName, payment.lastName,
                payment.address1, payment.address2, payment.locality, payment.administrativeArea, payment.postalCode,
                payment.country, payment.email, payment.phoneNumber);

        Card cardInfo = new Card(payment.number, payment.expirationMonth, payment.expirationYear, payment.securityCode);

        //set the client reference info
        System.out.println("payment: "+payment);
        System.out.println("payment.code: "+payment.code);
        body.clientReferenceInformation = new ClientReferenceInformation(payment.code);

        //set the order Info
        body.orderInformation = new Request.OrderInformation(amountDetail,billTo);

        //set the payment Info
        body.paymentInformation = new Request.PaymentInformation(cardInfo);
        body.processingInformation = new Request.ProcessingInformation("internet");

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        requestBodyString = objectMapper.writeValueAsString(body);
        System.out.println("requestBodyString: "+requestBodyString);

        // POST Example for Payments
        postRequestTarget = "post /pts/v2/payments";
        APINAME = "payments";
        resource = "/pts/v2/payments";

        payload = requestBodyString;

        System.out.println("\n\nSample 1: POST call - CyberSource Payments API - HTTP POST Payment request");
        return http.sendPost("https://" + requestHost + resource);
    }

    private String sendPost(String url) throws Exception
    {
        /* HTTP connection */
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        /* Add Request Header
         * "v-c-merchant-id" set value to Cybersource Merchant ID or v-c-merchant-id
         * This ID can be found on EBC portal
         */
        con.setRequestProperty("v-c-merchant-id", merchantId);
        con.setRequestProperty("v-c-correlation-id", "123");
        //con.setRequestProperty("profile-id", "93B32398-AD51-4CC2-A682-EA3E93614EB1");

        // Oleg's profile-ID
        // con.setRequestProperty("profile-id", "DE14A8CD-CC3E-4889-AC46-67ACFB7CD27C");

        /* Add Request Header
         * "Date" The date and time that the message was originated from.
         * "HTTP-date" format as defined by RFC7231.
         */
        gmtDateTime = getdate();

        con.setRequestProperty("date", gmtDateTime);

        /* Add Request Header
         * "Host" Name of the host to send the request to.
         */
        con.setRequestProperty("Host", requestHost);

        /* Add Request Header
         * "Digest" SHA-256 hash of payload that is BASE64 encoded
         */
        con.setRequestProperty("Digest", getDigest());

        /* Add Request Header
         * "Signature" Contains keyId, algorithm, headers and signature as paramters
         * Check getSignatureHeader() method for more details
         */
        StringBuilder signatureHeaderValue = getSignatureHeader("POST");

        con.setRequestProperty("Signature", signatureHeaderValue.toString());

        /* HTTP Method POST */
        con.setRequestMethod("POST");

        /* Additional Request Headers */
        //con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "application/json");

        // Send POST request
        con.setDoOutput(true);
        con.setDoInput(true);

        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.write(getPayload().getBytes("UTF-8"));
        wr.flush();
        wr.close();

        /* Establishing HTTP connection*/
        int responseCode = con.getResponseCode();

        String responseHeader = con.getHeaderField("v-c-correlation-id");
        System.out.println("\n -- RequestURL -- ");
        System.out.println("\tURL : " + url);
        System.out.println("\n -- HTTP Headers -- ");
        System.out.println("\tContent-Type : " + "application/json");
        System.out.println("\tv-c-merchant-id : " + merchantId);
        System.out.println("\tDate : " + gmtDateTime);
        System.out.println("\tHost : " + requestHost);
        System.out.println("\tDigest : " + getDigest());
        System.out.println("\tSignature : " + signatureHeaderValue);
        System.out.println("\n -- Response Message -- " );
        System.out.println("\tResponse Code :" + responseCode);
        System.out.println("\tv-c-correlation-id :" + responseHeader);

        try {
            /* Reading Response Message */
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();
            responseJSON = response.toString();
            /* print Response */
            System.out.println("\tResponse Payload :\n" + response.toString());
        } catch (Exception exception) {
            // Output unexpected IOExceptions
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();
            responseJSON = response.toString();
            /* print Response */
            System.out.println("Response Payload : " + response.toString());
            System.out.println(exception);
        }

        return responseJSON;
    }

    private String getdate()
    {
        /*  This Method returns Date in GMT format as defined by RFC7231. */
        return(DateTimeFormatter.RFC_1123_DATE_TIME.format(ZonedDateTime.now(ZoneId.of("GMT"))));
    }

    private StringBuilder getSignatureHeader(String httpMethod) throws Exception
    {
        /* This method return SignatureHeader Value that contains following paramters
         * keyid -- Merchant ID obtained from EBC portal
         * algorithm -- Should have value as "HmacSHA256"
         * headers -- List of all header name passed in the Signature paramter below
         *            String getHeaders = "host date (request-target)" + " " + "v-c-merchant-id";
         *            String postHeaders = "host date (request-target) digest v-c-merchant-id";
         *            Note: Digest is not passed for GET calls
         * signature -- Signature header has paramter called signature
         *              Paramter 'Signature' must contain all the paramters mentioned in header above in given order
         */
        StringBuilder signatureHeaderValue = new StringBuilder();

        /* KeyId is the key obtained from EBC */
        signatureHeaderValue.append("keyid=\"" + merchantKeyId + "\"");


        /* Algorithm should be always HmacSHA256 for http signature */
        signatureHeaderValue.append(", algorithm=\"HmacSHA256\"");

        /* Headers - list is choosen based on HTTP method. Digest is not required for GET Method */
        String getHeaders = "host date (request-target)" + " " + "v-c-merchant-id";
        String postHeaders = "host date (request-target) digest v-c-merchant-id";

        if(httpMethod.equalsIgnoreCase("GET"))
            signatureHeaderValue.append(", headers=\"" + getHeaders + "\"");
        else if(httpMethod.equalsIgnoreCase("POST"))
            signatureHeaderValue.append(", headers=\"" + postHeaders + "\"");

        /* Get Value for paramter 'Signature' to be passed to Signature Header */
        String signatureValue = getSignatureParam(httpMethod);
        signatureHeaderValue.append(", signature=\"" + signatureValue + "\"");

        return signatureHeaderValue;
    }

    private String getSignatureParam(String httpMethod)  throws Exception
    {
        /* This method returns value for paramter Signature which is then passed to Signature header
         * paramter 'Signature' is calculated based on below key values and then signed with SECRET KEY -
         * host: Sandbox (apitest.cybersource.com) or Production (api.cybersource.com) hostname
         * date: "HTTP-date" format as defined by RFC7231.
         * (request-target): Should be in format of httpMethod: path
         *                   Example: "post /pts/v2/payments"
         * Digest: Only needed for POST calls.
         *          digestString = BASE64( HMAC-SHA256 ( Payload ));
         *          Digest: “SHA-256=“ + digestString;
         * v-c-merchant-id: set value to Cybersource Merchant ID
         *                   This ID can be found on EBC portal
         */
        StringBuilder signatureString = new StringBuilder();
        signatureString.append('\n');
        signatureString.append("host");
        signatureString.append(": ");
        signatureString.append(requestHost);
        signatureString.append('\n');
        signatureString.append("date");
        signatureString.append(": ");
        signatureString.append(gmtDateTime);
        signatureString.append('\n');
        signatureString.append("(request-target)");
        signatureString.append(": ");

        String getRequestTarget = "get " + resource;

        if(httpMethod.equalsIgnoreCase("GET"))
            signatureString.append(getRequestTarget);
        else if(httpMethod.equalsIgnoreCase("POST"))
            signatureString.append(postRequestTarget);

        signatureString.append('\n');

        if(httpMethod.equalsIgnoreCase("POST")) {
            signatureString.append("digest");
            signatureString.append(": ");
            signatureString.append(getDigest());
            signatureString.append('\n');
        }

        signatureString.append("v-c-merchant-id");
        signatureString.append(": ");
        signatureString.append(merchantId);
        signatureString.delete(0, 1);

        String signatureStr = signatureString.toString();

        /* Signature string generated from above parameters is Signed with SecretKey hased with SHA256 and base64 encoded.
         *  Secret Key is Base64 decoded before signing
         */
        SecretKeySpec secretKey = new SecretKeySpec(Base64.getDecoder().decode(merchantsecretKey), "HmacSHA256");
        Mac aKeyId = Mac.getInstance("HmacSHA256");
        aKeyId.init(secretKey);
        aKeyId.update(signatureStr.getBytes());
        byte[] aHeaders = aKeyId.doFinal();
        String base64EncodedSignature = Base64.getEncoder().encodeToString(aHeaders);

        return base64EncodedSignature;
    }

    private String getDigest() throws NoSuchAlgorithmException, IOException
    {
        /* This method return Digest value which is SHA-256 hash of payload that is BASE64 encoded */
        String messageBody = getPayload();

        MessageDigest digestString = MessageDigest.getInstance("SHA-256");

        byte[] digestBytes = digestString.digest(messageBody.getBytes("UTF-8"));

        String bluePrint = Base64.getEncoder().encodeToString(digestBytes);
        bluePrint = "SHA-256="+ bluePrint;

        return bluePrint;
    }

    private String getPayload() throws IOException
    {
        String messageBody = payload;
        return messageBody;
    }

    public static class Payment
    {
        private static String merchantId;
        private static String keyId;
        private static String secretKey;
        private String code;
        private String totalAmount;
        private String currency;
        private String number;
        private String expirationMonth;
        private String expirationYear;
        private String securityCode;
        private String firstName;
        private String lastName;
        private String address1;
        private String address2;
        private String locality;
        private String administrativeArea;
        private String postalCode;
        private String country;
        private String email;
        private String phoneNumber;

        public Payment() {}
        public Payment(String merchantId, String keyId, String secretKey,
                String code, String totalAmount, String currency,
                String number, String expMonth, String expYear, String securityCode,
                String billingFirstName, String billingLastName,
                String billingStreet, String billingStreet2, String billingCity, String billingState,
                String billingPostalCode, String billingCountry,
                String billingEmail, String billingPhone)
        {
            super();
            this.merchantId = merchantId;
            this.keyId = keyId;
            this.secretKey = secretKey;
            this.code = code;
            this.totalAmount = totalAmount;
            this.currency = currency;
            this.number = number;
            this.expirationMonth = expMonth;
            this.expirationYear = expYear;
            this.securityCode = securityCode;
            this.firstName = billingFirstName;
            this.lastName = billingLastName;
            this.address1 = billingStreet;
            this.address2 = billingStreet2;
            this.locality = billingCity;
            this.administrativeArea = billingState;
            this.postalCode = billingPostalCode;
            this.country = billingCountry;
            this.email = billingEmail;
            this.phoneNumber = billingPhone;
        }

        public String getMerchantId() { return this.merchantId; }
        public void setMerchantId(String merchantId) { this.merchantId = merchantId; }

        public String getKeyId() { return this.keyId; }
        public void setKeyId(String keyId) { this.keyId = keyId; }

        public String getSecretKey() { return this.secretKey; }
        public void setSecretKey(String secretKey) { this.secretKey = secretKey; }

        public String getCode() { return this.code; }
        public void setCode(String code) { this.code = code; }

        public String getTotalAmount() { return this.totalAmount; }
        public void setTotalAmount(String totalAmount) { this.totalAmount = totalAmount; }

        public String getCurrency() { return this.currency; }
        public void setCurrency(String currency) { this.currency = currency; }

        public String getNumber() { return this.number; }
        public void setNumber(String number) { this.number = number; }

        public String getExpirationMonth() { return this.expirationMonth; }
        public void setExpirationMonth(String expirationMonth) { this.expirationMonth = expirationMonth; }

        public String getExpirationYear() { return this.expirationYear; }
        public void setExpirationYear(String expirationYear) { this.expirationYear = expirationYear; }

        public String getSecurityCode() { return this.securityCode; }
        public void setSecurityCode(String securityCode) { this.securityCode = securityCode; }

        public String getFirstName() { return this.firstName; }
        public void setFirstName(String firstName) { this.firstName = firstName; }

        public String getLastName() { return this.lastName; }
        public void setLastName(String lastName) { this.lastName = lastName; }

        public String getAddress1() { return this.address1; }
        public void setAddress1(String address1) { this.address1 = address1; }

        public String getAddress2() { return this.address2; }
        public void setAddress2(String address2) { this.address2 = address2; }

        public String getLocality() { return this.locality; }
        public void setLocality(String locality) { this.locality = locality; }

        public String getAdministrativeArea() { return this.administrativeArea; }
        public void setAdministrativeArea(String administrativeArea) { this.administrativeArea = administrativeArea; }

        public String getPostalCode() { return this.postalCode; }
        public void setPostalCode(String postalCode) { this.postalCode = postalCode; }

        public String getCountry() { return this.country; }
        public void setCountry(String country) { this.country = country; }

        public String getEmail() { return this.email; }
        public void setEmail(String email) { this.email = email; }

        public String getPhoneNumber() { return this.phoneNumber; }
        public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

        @Override
        public String toString()
        {
            return "Payment [merchantId=" + merchantId + ",  keyId=" + keyId + ",  secretKey=" + secretKey + ", code=" + code + ", totalAmount=" + totalAmount + ", currency=" + currency + ", number=" + number + ", expirationMonth=" + expirationMonth + ", expirationYear=" + expirationYear + ", securityCode=" + securityCode + ", firstName=" + firstName + ", lastName=" + lastName + ", address1=" + address1 + ", address2=" + address2 + ", locality=" + locality + ", administrativeArea=" + administrativeArea + ", postalCode=" + postalCode + ", country=" + country + ", email=" + email + ", phoneNumber=" + phoneNumber + "]";
        }
    }

    public static class Body
    {
        private ClientReferenceInformation clientReferenceInformation; //optional
        private ProcessingInformation processingInformation; //optional
        private PaymentInformation paymentInformation;
        private OrderInformation orderInformation;
    }

    public static class ClientReferenceInformation
    {
        public String code;
        public ClientReferenceInformation(){}
        public ClientReferenceInformation(String code)
        {
            this.code = code;
        }
    }

    public static class ProcessingInformation
    {
        public String commerceIndicator;
        public ProcessingInformation(){}
        public ProcessingInformation(String commerceIndicator)
        {
            this.commerceIndicator = commerceIndicator;
        }
    }

    public static class PaymentInformation
    {
        public Card card;
        public PaymentInformation(){}
        public PaymentInformation(Card card)
        {
            this.card = card;
        }
    }

    public static class OrderInformation
    {
        public AmountDetails amountDetails;
        public BillTo billTo;
        public OrderInformation(){}
        public OrderInformation(AmountDetails amountDetails, BillTo billTo)
        {
            this.amountDetails = amountDetails;
            this.billTo = billTo;
        }
    }

    public static class Card
    {
        public String number;
        public String expirationMonth;
        public String expirationYear;
        public String securityCode;

        public Card(){}
        public Card(String number, String expirationMonth, String expirationYear,
                    String securityCode)
        {
            this.number = number;
            this.expirationMonth = expirationMonth;
            this.expirationYear = expirationYear;
            this.securityCode = securityCode;
        }
    }

    public static class AmountDetails
    {
        public String totalAmount;
        public String currency;
        public AmountDetails(){}
        public AmountDetails(String totalAmount, String currency)
        {
            this.totalAmount = totalAmount;
            this.currency = currency;
        }
    }

    public static class BillTo
    {
        public String firstName;
        public String lastName;
        public String address1;
        public String address2;
        public String locality;
        public String administrativeArea;
        public String postalCode;
        public String country;
        public String email;
        public String phoneNumber;
        public BillTo(){}
        public BillTo(String firstName, String lastName,
                      String address1, String address2, String locality, String administrativeArea, String postalCode,
                      String country, String email, String phoneNumber)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.address1 = address1;
            this.address2 = address2;
            this.locality = locality;
            this.administrativeArea = administrativeArea;
            this.postalCode = postalCode;
            this.country = country;
            this.email = email;
            this.phoneNumber = phoneNumber;
        }
    }
}